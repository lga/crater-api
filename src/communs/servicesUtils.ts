export interface Entite {
    readonly id: string;
}

export class StockageMemoireIndexParId<T extends Entite> {
    private entites: Map<string, T> = new Map<string, T>();

    findAll(): Array<T> {
        return [...this.entites.values()];
    }

    save(entite: T): void {
        this.entites.set(entite.id, entite);
    }

    findById(id: string): T | undefined {
        return this.entites.get(id);
    }
}
