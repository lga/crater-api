import { ErreurTechnique, ErreurTemporairementIndisponible } from './erreurs';

export enum Etat {
    INIT_EN_COURS,
    PRET,
    ERREUR
}

export function verifierEtatApi(etat: Etat) {
    if (etat === Etat.INIT_EN_COURS)
        throw new ErreurTemporairementIndisponible('Initialisation en cours, réessayer dans quelques instants.');
    if (etat === Etat.ERREUR) throw new ErreurTechnique('Erreur technique inconnue.');
}

export const SAUT_LIGNE = '\n';

export function jsonToCsv(objetJson: any, prefixeNomChamp: string = '', ignorerNomChamp: boolean = true): string[] {
    let csv: string[] = [];
    for (let nomChampCourant in objetJson) {
        let nomCompletChamp = calculerNomCompletChamp(objetJson, prefixeNomChamp, nomChampCourant);
        let valeurChampCourant = objetJson[nomChampCourant];

        if (estTableauDeValeurs(valeurChampCourant)) {
            csv.push(nomCompletChamp + ';' + valeurChampCourant.join(';'));
        } else if (estObjetOuTableauDObjets(valeurChampCourant)) {
            csv.push(...jsonToCsv(valeurChampCourant, nomCompletChamp));
        } else {
            // cas champ avec une valeur simple : string/number/boolean
            csv.push(nomCompletChamp + ';' + valeurChampCourant);
        }
    }
    return csv;

    function calculerNomCompletChamp(objetJson: any, prefixeNomChamp: string, nomChampCourant: string) {
        if (Array.isArray(objetJson)) {
            return prefixeNomChamp;
        } else {
            return (prefixeNomChamp ? prefixeNomChamp + '.' : '') + nomChampCourant;
        }
    }

    function estObjetOuTableauDObjets(valeurChamp: any) {
        return !estTableauDeValeurs(valeurChamp) && typeof valeurChamp === 'object';
    }

    function estTableauDeValeurs(valeurChamp: any) {
        return (
            typeof valeurChamp === 'object' &&
            Array.isArray(valeurChamp) &&
            valeurChamp.length > 0 &&
            (!valeurChamp[0] ||
                typeof valeurChamp[0] === 'number' ||
                typeof valeurChamp[0] === 'string' ||
                typeof valeurChamp[0] === 'boolean')
        );
    }
}

export function extraireValeurDepuisSousObjet(
    objet: any,
    cheminChampAExtraire: string,
    anneeAConserver: string = ''
): any {
    let cheminObjetParent = cheminChampAExtraire.split('.').slice(0, -1).join('.');
    let objetParent = extraireSousObjet(objet, cheminObjetParent);

    if (!Array.isArray(objetParent)) {
        return extraireSousObjet(objet, cheminChampAExtraire);
    } else {
        let nomChampFiltre = '';
        let valeurChampFiltre = '';
        if (anneeAConserver != '') {
            nomChampFiltre = 'annee';
            valeurChampFiltre = anneeAConserver;
        }
        return extrairePremiereValeurSelonFiltre(objet, cheminChampAExtraire, nomChampFiltre, valeurChampFiltre);
    }
}

export function extraireSousObjet(objet: any, cheminSousObjet: string): any {
    const listeCles = cheminSousObjet.split('.');
    let sousObjet = objet;
    for (let cle of listeCles) {
        if (sousObjet.hasOwnProperty(cle)) {
            sousObjet = sousObjet[cle];
        } else {
            return {};
        }
    }
    return sousObjet;
}

export function extrairePremiereValeurSelonFiltre(
    objet: any,
    cheminChampAExtraire: string,
    nomChampFiltre: string,
    valeurFiltre: string
): (string | number) | (string | number)[] | null {
    let cheminObjetParent = cheminChampAExtraire.split('.').slice(0, -1).join('.');
    let nomChampAExtraire = cheminChampAExtraire.split('.').slice(-1)[0];
    let tableauParent = extraireSousObjet(objet, cheminObjetParent);

    if (!Array.isArray(tableauParent)) return null;

    let premierObjetSelonFiltre = tableauParent.find((o) => {
        return !o.hasOwnProperty(nomChampFiltre) || o[nomChampFiltre].toString() === valeurFiltre;
    });
    if (premierObjetSelonFiltre.hasOwnProperty(nomChampAExtraire)) {
        if (nomChampFiltre == '') return tableauParent.map((o) => o[nomChampAExtraire]);
        else return premierObjetSelonFiltre[nomChampAExtraire];
    }
    return null;
}

export function convertirDateEnString(date: Date): string {
    return new Date().toISOString().split('T')[0];
}
