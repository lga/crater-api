export class ErreurRessourceNonTrouvee extends Error {
    constructor(message: string | undefined) {
        super(message);
    }
}

export class ErreurRequeteIncorrecte extends Error {
    constructor(message: string | undefined) {
        super(message);
    }
}

export class ErreurTemporairementIndisponible extends Error {
    constructor(message: string | undefined) {
        super(message);
    }
}

export class ErreurTechnique extends Error {
    constructor(message: string | undefined) {
        super(message);
    }
}
