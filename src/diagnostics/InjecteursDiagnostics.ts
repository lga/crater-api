import {
    InjecteurCsv,
    lireCelluleFloatOuNull,
    lireCelluleInt,
    lireCelluleIntOuNull,
    lireCelluleString,
    TraiterCellules
} from '../communs/injecteursUtils';
import { ServicesDiagnostics } from './domaines/ServicesDiagnostics';
import {
    BesoinsParCultureEntite,
    BesoinsParGroupeCultureEntite,
    DiagnosticEntite,
    SauParCultureEntite,
    SauParGroupeCultureEntite,
    ValeursParClassesAgesEntite,
    ValeursParClassesSuperficiesEntite
} from './domaines/entitesDiagnostics';
import { CATEGORIE_COMMUNE } from '../territoires/domaines/entitesTerritoires';
import { MapIdsTerritoires } from '../communs/MapIdsTerritoires';

export class InjecteursDiagnostics {
    private readonly idsTerritoires = new MapIdsTerritoires();

    constructor(private readonly servicesDiagnostics: ServicesDiagnostics) {}

    private traiterCellulesReferentielTerritoires: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        diagnostic.idDepartement = this.idsTerritoires.lireCelluleIdTerritoire(mapCellules, 'id_departement');
    };

    private traiterCellulesDonneesTerritoires: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        diagnostic.nbCommunes = lireCelluleIntOuNull(mapCellules, 'nb_communes');
        diagnostic.population = lireCelluleIntOuNull(mapCellules, 'population_totale_2017');
        diagnostic.superficieHa = lireCelluleIntOuNull(mapCellules, 'superficie_ha');
        diagnostic.surfaceAgricoleUtile.sauTotaleHa = lireCelluleIntOuNull(mapCellules, 'sau_ha');
        diagnostic.surfaceAgricoleUtile.sauProductiveHa = lireCelluleIntOuNull(mapCellules, 'sau_productive_ha');
        diagnostic.surfaceAgricoleUtile.sauPeuProductiveHa = lireCelluleIntOuNull(mapCellules, 'sau_peu_productive_ha');
        diagnostic.idTerritoireParcel = lireCelluleString(mapCellules, 'id_territoire_parcel');
    };

    private traiterCellulesOtexTerritoires: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        diagnostic.nbCommunesParOtex.grandesCultures = lireCelluleInt(mapCellules, 'nb_communes_otex_grandes_cultures');
        diagnostic.nbCommunesParOtex.maraichageHorticulture = lireCelluleInt(
            mapCellules,
            'nb_communes_otex_maraichage_horticulture'
        );
        diagnostic.nbCommunesParOtex.viticulture = lireCelluleInt(mapCellules, 'nb_communes_otex_viticulture');
        diagnostic.nbCommunesParOtex.fruits = lireCelluleInt(mapCellules, 'nb_communes_otex_fruits');
        diagnostic.nbCommunesParOtex.bovinLait = lireCelluleInt(mapCellules, 'nb_communes_otex_bovin_lait');
        diagnostic.nbCommunesParOtex.bovinViande = lireCelluleInt(mapCellules, 'nb_communes_otex_bovin_viande');
        diagnostic.nbCommunesParOtex.bovinMixte = lireCelluleInt(mapCellules, 'nb_communes_otex_bovin_mixte');
        diagnostic.nbCommunesParOtex.ovinsCaprinsAutresHerbivores = lireCelluleInt(
            mapCellules,
            'nb_communes_otex_ovins_caprins_autres_herbivores'
        );
        diagnostic.nbCommunesParOtex.porcinsVolailles = lireCelluleInt(
            mapCellules,
            'nb_communes_otex_porcins_volailles'
        );
        diagnostic.nbCommunesParOtex.polyculturePolyelevage = lireCelluleInt(
            mapCellules,
            'nb_communes_otex_polyculture_polyelevage'
        );
        diagnostic.nbCommunesParOtex.nonClassees = lireCelluleInt(mapCellules, 'nb_communes_otex_non_classees');
        diagnostic.nbCommunesParOtex.sansExploitations = lireCelluleInt(
            mapCellules,
            'nb_communes_otex_sans_exploitations'
        );
        diagnostic.nbCommunesParOtex.nonRenseigne = lireCelluleInt(mapCellules, 'nb_communes_otex_na');
    };
    private traiterCellulesPolitiqueFonciere: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        diagnostic.politiqueFonciere.note = lireCelluleIntOuNull(mapCellules, 'note');
        diagnostic.politiqueFonciere.artificialisation5ansHa = lireCelluleFloatOuNull(
            mapCellules,
            'artificialisation_2011_2016_ha'
        );
        diagnostic.politiqueFonciere.evolutionMenagesEmplois5ans = lireCelluleFloatOuNull(
            mapCellules,
            'evolution_menages_emplois_2011_2016'
        );
        diagnostic.politiqueFonciere.sauParHabitantM2 = lireCelluleFloatOuNull(
            mapCellules,
            'sau_par_habitant_valeur_m2_par_hab'
        );
        diagnostic.politiqueFonciere.rythmeArtificialisationSauPourcent = lireCelluleFloatOuNull(
            mapCellules,
            'rythme_artificialisation_sau_pourcent'
        );
        diagnostic.politiqueFonciere.politiqueAmenagement = lireCelluleString(
            mapCellules,
            'evaluation_politique_amenagement'
        );
        diagnostic.politiqueFonciere.partLogementsVacants2013Pourcent = lireCelluleFloatOuNull(
            mapCellules,
            'part_logements_vacants_2013_pourcent'
        );
        diagnostic.politiqueFonciere.partLogementsVacants2018Pourcent = lireCelluleFloatOuNull(
            mapCellules,
            'part_logements_vacants_2018_pourcent'
        );
    };

    private traiterCellulesPopulationAgricole: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        diagnostic.populationAgricole.note = lireCelluleIntOuNull(mapCellules, 'note');
        diagnostic.populationAgricole.populationAgricole1988 = lireCelluleIntOuNull(
            mapCellules,
            'actifs_agricoles_permanents_1988'
        );
        diagnostic.populationAgricole.populationAgricole2010 = lireCelluleIntOuNull(
            mapCellules,
            'actifs_agricoles_permanents_2010'
        );
        diagnostic.populationAgricole.partPopulationAgricole1988Pourcent = lireCelluleFloatOuNull(
            mapCellules,
            'part_population_agricole_1988_pourcent'
        );
        diagnostic.populationAgricole.partPopulationAgricole2010Pourcent = lireCelluleFloatOuNull(
            mapCellules,
            'part_population_agricole_2010_pourcent'
        );
        diagnostic.populationAgricole.nbExploitations1988 = lireCelluleIntOuNull(mapCellules, 'nb_exploitations_1988');
        diagnostic.populationAgricole.nbExploitations2010 = lireCelluleIntOuNull(mapCellules, 'nb_exploitations_2010');
        diagnostic.populationAgricole.sauHa1988 = lireCelluleIntOuNull(mapCellules, 'sau_ha_1988');
        diagnostic.populationAgricole.sauHa2010 = lireCelluleIntOuNull(mapCellules, 'sau_ha_2010');
        diagnostic.populationAgricole.sauMoyenneParExploitationHa1988 = lireCelluleIntOuNull(
            mapCellules,
            'sau_moyenne_par_exploitation_ha_1988'
        );
        diagnostic.populationAgricole.sauMoyenneParExploitationHa2010 = lireCelluleIntOuNull(
            mapCellules,
            'sau_moyenne_par_exploitation_ha_2010'
        );

        diagnostic.populationAgricole.nbExploitationsParClassesAgesChefExploitation = new ValeursParClassesAgesEntite(
            lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_ages_chef_exploitation.moins_40_ans'),
            lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_ages_chef_exploitation.40_a_49_ans'),
            lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_ages_chef_exploitation.50_a_59_ans'),
            lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_ages_chef_exploitation.plus_60_ans')
        );
        diagnostic.populationAgricole.nbExploitationsParClassesSuperficies = new ValeursParClassesSuperficiesEntite(
            lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_superficies.moins_20_ha'),
            lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_superficies.20_a_50_ha'),
            lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_superficies.50_a_100_ha'),
            lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_superficies.100_a_200_ha'),
            lireCelluleIntOuNull(mapCellules, 'nb_exploitations_par_classes_superficies.plus_200_ha')
        );
        diagnostic.populationAgricole.sauHaParClassesSuperficies = new ValeursParClassesSuperficiesEntite(
            lireCelluleIntOuNull(mapCellules, 'sau_ha_par_classes_superficies.moins_20_ha'),
            lireCelluleIntOuNull(mapCellules, 'sau_ha_par_classes_superficies.20_a_50_ha'),
            lireCelluleIntOuNull(mapCellules, 'sau_ha_par_classes_superficies.50_a_100_ha'),
            lireCelluleIntOuNull(mapCellules, 'sau_ha_par_classes_superficies.100_a_200_ha'),
            lireCelluleIntOuNull(mapCellules, 'sau_ha_par_classes_superficies.plus_200_ha')
        );
    };

    private traiterCellulesIntrants: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        // on n'importe pas les données de pesticides à l'échelle des communes (manque de fiabilité) et du département de Paris qui correspond à une commune
        if (this.suppressionDonneesCommunales(mapCellules)) {
            diagnostic.intrants.nodeNormalise = null;
            diagnostic.intrants.note = null;
        } else {
            diagnostic.intrants.nodeNormalise = lireCelluleFloatOuNull(mapCellules, 'NODU_normalise');
            diagnostic.intrants.note = lireCelluleIntOuNull(mapCellules, 'note');
        }
    };

    private traiterCellulesPesticidesMoyennesTriennales: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        // on n'importe pas les données de pesticides à l'échelle des communes (manque de fiabilité) et du département de Paris qui correspond à une commune
        if (this.suppressionDonneesCommunales(mapCellules)) {
            diagnostic.intrants.pesticides.indicateursMoyennesTriennales.ajouterValeursAnneeN(
                lireCelluleInt(mapCellules, 'annee'),
                null,
                null,
                null,
                null
            );
        } else {
            diagnostic.intrants.pesticides.indicateursMoyennesTriennales.ajouterValeursAnneeN(
                lireCelluleInt(mapCellules, 'annee'),
                lireCelluleFloatOuNull(mapCellules, 'quantite_substance_sans_du_kg'),
                lireCelluleFloatOuNull(mapCellules, 'quantite_substance_avec_du_kg'),
                lireCelluleFloatOuNull(mapCellules, 'NODU_ha'),
                lireCelluleFloatOuNull(mapCellules, 'NODU_normalise')
            );
        }
    };

    private traiterCellulesPratiquesAgricoles: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        diagnostic.pratiquesAgricoles.indicateurHvn.indice1 = lireCelluleFloatOuNull(mapCellules, 'hvn_indice1');
        diagnostic.pratiquesAgricoles.indicateurHvn.indice2 = lireCelluleFloatOuNull(mapCellules, 'hvn_indice2');
        diagnostic.pratiquesAgricoles.indicateurHvn.indice3 = lireCelluleFloatOuNull(mapCellules, 'hvn_indice3');
        diagnostic.pratiquesAgricoles.indicateurHvn.indiceTotal = lireCelluleFloatOuNull(mapCellules, 'hvn_score');
        diagnostic.pratiquesAgricoles.partSauBioPourcent = lireCelluleFloatOuNull(mapCellules, 'part_sau_bio_pourcent');
        diagnostic.surfaceAgricoleUtile.sauBioHa = lireCelluleFloatOuNull(mapCellules, 'surface_bio_ha');
    };

    private traiterCellulesSauParCulture: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        const sauParGroupeCulture = this.ajouterSauParGroupeCulture(diagnostic, mapCellules);
        this.ajouterSauParCulture(sauParGroupeCulture, mapCellules);
    };

    private traiterCellulesConsommation: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        diagnostic.consommation.tauxPauvrete60Pourcent = lireCelluleFloatOuNull(
            mapCellules,
            'taux_pauvrete_60_pourcent'
        );
    };

    private ajouterSauParGroupeCulture(diagnostic: DiagnosticEntite, mapCellules: Map<string, string>) {
        let sauParGroupeCulture = diagnostic.surfaceAgricoleUtile.sauParGroupeCulture.find(
            (s) => s.codeGroupeCulture === lireCelluleString(mapCellules, 'code_groupe_culture')
        );
        if (!sauParGroupeCulture) {
            sauParGroupeCulture = new SauParGroupeCultureEntite(
                lireCelluleString(mapCellules, 'code_groupe_culture'),
                lireCelluleString(mapCellules, 'nom_groupe_culture'),
                lireCelluleFloatOuNull(mapCellules, 'production_groupe_culture_ha')
            );
            diagnostic.surfaceAgricoleUtile.sauParGroupeCulture.push(sauParGroupeCulture);
        }
        return sauParGroupeCulture;
    }

    private ajouterSauParCulture(sauParGroupeCulture: any, mapCellules: Map<string, string>) {
        const sauParCulture = new SauParCultureEntite(
            lireCelluleString(mapCellules, 'code_culture'),
            lireCelluleString(mapCellules, 'nom_culture'),
            lireCelluleFloatOuNull(mapCellules, 'production_culture_ha')
        );
        sauParGroupeCulture.sauParCulture.push(sauParCulture);
    }

    private traiterCellulesProductionsBesoins: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        diagnostic.productionsBesoins.besoinsAssietteActuelle.besoinsHa = lireCelluleFloatOuNull(
            mapCellules,
            'besoins_assiette_actuelle_ha'
        );
        diagnostic.productionsBesoins.besoinsAssietteDemitarienne.besoinsHa = lireCelluleFloatOuNull(
            mapCellules,
            'besoins_assiette_demitarienne_ha'
        );
        diagnostic.productionsBesoins.besoinsAssietteActuelle.tauxAdequationBrutPourcent = lireCelluleFloatOuNull(
            mapCellules,
            'taux_adequation_brut_assiette_actuelle_pourcent'
        );
        diagnostic.productionsBesoins.besoinsAssietteActuelle.tauxAdequationMoyenPonderePourcent =
            lireCelluleFloatOuNull(mapCellules, 'taux_adequation_moyen_pondere_assiette_actuelle_pourcent');

        diagnostic.productionsBesoins.besoinsAssietteDemitarienne.tauxAdequationBrutPourcent = lireCelluleFloatOuNull(
            mapCellules,
            'taux_adequation_brut_assiette_demitarienne_pourcent'
        );
        diagnostic.productionsBesoins.besoinsAssietteDemitarienne.tauxAdequationMoyenPonderePourcent =
            lireCelluleFloatOuNull(mapCellules, 'taux_adequation_moyen_pondere_assiette_demitarienne_pourcent');
    };

    private traiterCellulesProductionsBesoinsParGroupeCulture: TraiterCellules = (mapCellules: Map<string, string>) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        const besoinsParGroupeCultureAssietteActuelle = new BesoinsParGroupeCultureEntite(
            lireCelluleString(mapCellules, 'code_groupe_culture'),
            lireCelluleString(mapCellules, 'nom_groupe_culture'),
            lireCelluleFloatOuNull(mapCellules, 'besoins_assiette_actuelle_ha'),
            lireCelluleFloatOuNull(mapCellules, 'taux_adequation_brut_assiette_actuelle_pourcent')
        );

        diagnostic.productionsBesoins.besoinsAssietteActuelle.besoinsParGroupeCulture.push(
            besoinsParGroupeCultureAssietteActuelle
        );

        const besoinsParGroupeCultureAssietteDemitarienne = new BesoinsParGroupeCultureEntite(
            lireCelluleString(mapCellules, 'code_groupe_culture'),
            lireCelluleString(mapCellules, 'nom_groupe_culture'),
            lireCelluleFloatOuNull(mapCellules, 'besoins_assiette_demitarienne_ha'),
            lireCelluleFloatOuNull(mapCellules, 'taux_adequation_brut_assiette_demitarienne_pourcent')
        );

        diagnostic.productionsBesoins.besoinsAssietteDemitarienne.besoinsParGroupeCulture.push(
            besoinsParGroupeCultureAssietteDemitarienne
        );
    };

    private traiterCellulesBesoinsParCulture: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        this.ajouterBesoinsParCulture(
            diagnostic.productionsBesoins.besoinsAssietteActuelle.besoinsParGroupeCulture,
            mapCellules,
            'assiette_actuelle'
        );
        this.ajouterBesoinsParCulture(
            diagnostic.productionsBesoins.besoinsAssietteDemitarienne.besoinsParGroupeCulture,
            mapCellules,
            'assiette_demitarienne'
        );
    };

    private ajouterBesoinsParCulture(
        besoinsParGroupeCulture: BesoinsParGroupeCultureEntite[],
        mapCellules: Map<string, string>,
        infixeTypeAssiette: string
    ) {
        let codeGroupeCultureCourant = lireCelluleString(mapCellules, 'code_groupe_culture');
        let besoinsParGroupeCulturePourGroupeCultureCourant: BesoinsParGroupeCultureEntite =
            besoinsParGroupeCulture.find(
                (s: { codeGroupeCulture: string }) => s.codeGroupeCulture === codeGroupeCultureCourant
            )!;
        if (besoinsParGroupeCulturePourGroupeCultureCourant) {
            const besoinsParCulture = new BesoinsParCultureEntite(
                lireCelluleString(mapCellules, 'code_culture'),
                lireCelluleString(mapCellules, 'nom_culture'),
                'True' === lireCelluleString(mapCellules, 'alimentation_animale'),
                lireCelluleFloatOuNull(mapCellules, `besoins_${infixeTypeAssiette}_ha`)
            );

            besoinsParGroupeCulturePourGroupeCultureCourant.besoinsParCulture.push(besoinsParCulture);
        }
    }

    private traiterCellulesProduction: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        diagnostic.production.note = lireCelluleIntOuNull(mapCellules, 'note');
        diagnostic.production.tauxAdequationMoyenPonderePourcent = lireCelluleFloatOuNull(
            mapCellules,
            'taux_adequation_moyen_pondere_pourcent'
        );
        diagnostic.production.partSauBioPourcent = lireCelluleFloatOuNull(mapCellules, 'part_sau_bio_pourcent');
        diagnostic.production.indiceHvn = lireCelluleFloatOuNull(mapCellules, 'hvn_score');
        diagnostic.production.noteTauxAdequationMoyenPondere = lireCelluleIntOuNull(
            mapCellules,
            'taux_adequation_moyen_pondere_note'
        );
        diagnostic.production.notePartSauBio = lireCelluleIntOuNull(mapCellules, 'part_sau_bio_note');
        diagnostic.production.noteHvn = lireCelluleIntOuNull(mapCellules, 'hvn_note');
    };

    private traiterCellulesProximiteCommercesParTypesCommerces: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        diagnostic.proximiteCommerces.indicateursProximiteCommercesEntite.ajouterValeursPourUnTypeDeCommerce(
            lireCelluleString(mapCellules, 'code_type_commerce'),
            lireCelluleFloatOuNull(mapCellules, 'distance_plus_proche_commerce_m'),
            lireCelluleFloatOuNull(mapCellules, 'part_population_acces_a_pied_pourcent'),
            lireCelluleFloatOuNull(mapCellules, 'part_population_acces_a_velo_pourcent'),
            lireCelluleFloatOuNull(mapCellules, 'part_population_dependante_voiture_pourcent'),
            lireCelluleFloatOuNull(mapCellules, 'part_territoire_dependant_voiture_pourcent')
        );
    };

    private traiterCellulesProximiteCommerces: TraiterCellules = (mapCellules) => {
        const diagnostic = this.servicesDiagnostics.rechercherOuCreerDiagnostic(
            this.idsTerritoires.lireCelluleIdTerritoire(mapCellules),
            lireCelluleString(mapCellules, 'nom_territoire'),
            lireCelluleString(mapCellules, 'categorie_territoire')
        );
        diagnostic.proximiteCommerces.partPopulationDependanteVoiturePourcent = lireCelluleFloatOuNull(
            mapCellules,
            'part_population_dependante_voiture_pourcent'
        );
        diagnostic.proximiteCommerces.partTerritoireDependantVoiturePourcent = lireCelluleFloatOuNull(
            mapCellules,
            'part_territoire_dependant_voiture_pourcent'
        );
        diagnostic.proximiteCommerces.note = lireCelluleIntOuNull(mapCellules, 'note');
    };

    async injecterDonneesDiagnostics(dossierDonnees = '../crater-data/output-data') {
        await new InjecteurCsv().injecter(
            dossierDonnees + '/crater/territoires/referentiel_territoires/referentiel_territoires.csv',
            this.idsTerritoires.traiterCellulesCorrespondanceIdsTerritoires
        );

        await Promise.all([
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/territoires/referentiel_territoires/referentiel_territoires.csv',
                this.traiterCellulesReferentielTerritoires
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/territoires/donnees_territoires/donnees_territoires.csv',
                this.traiterCellulesDonneesTerritoires
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/territoires/donnees_territoires/otex_territoires.csv',
                this.traiterCellulesOtexTerritoires
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/politique_fonciere/politique_fonciere.csv',
                this.traiterCellulesPolitiqueFonciere
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/intrants_agricoles/intrants_agricoles.csv',
                this.traiterCellulesIntrants
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/intrants_agricoles/intrants_agricoles_moyennes_triennales.csv',
                this.traiterCellulesPesticidesMoyennesTriennales
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/population_agricole/population_agricole.csv',
                this.traiterCellulesPopulationAgricole
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/pratiques_agricoles/pratiques_agricoles.csv',
                this.traiterCellulesPratiquesAgricoles
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/productions_besoins/sau_par_culture.csv',
                this.traiterCellulesSauParCulture
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/productions_besoins/productions_besoins.csv',
                this.traiterCellulesProductionsBesoins
            ),
            this.injecterFichiersBesoinsParGroupesCultures(dossierDonnees),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/production/production.csv',
                this.traiterCellulesProduction
            ),

            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/proximite_commerces/proximite_commerces_par_types_commerces.csv',
                this.traiterCellulesProximiteCommercesParTypesCommerces
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/proximite_commerces/proximite_commerces.csv',
                this.traiterCellulesProximiteCommerces
            ),
            new InjecteurCsv().injecter(
                dossierDonnees + '/crater/consommation/consommation.csv',
                this.traiterCellulesConsommation
            )
        ]);
    }

    private async injecterFichiersBesoinsParGroupesCultures(dossierDonnees: string) {
        await new InjecteurCsv().injecter(
            dossierDonnees + '/crater/productions_besoins/productions_besoins_par_groupe_culture.csv',
            this.traiterCellulesProductionsBesoinsParGroupeCulture
        );
        await new InjecteurCsv().injecter(
            dossierDonnees + '/crater/productions_besoins/besoins_par_culture.csv',
            this.traiterCellulesBesoinsParCulture
        );
    }

    private suppressionDonneesCommunales(mapCellules: Map<string, string>) {
        return (
            lireCelluleString(mapCellules, 'categorie_territoire') === CATEGORIE_COMMUNE ||
            lireCelluleString(mapCellules, 'id_territoire') === 'D-75'
        );
    }
}
