import { StockageMemoireIndexParId } from '../../communs/servicesUtils';
import { DiagnosticEntite } from './entitesDiagnostics';

export class ServicesDiagnostics {
    private stockageDiagnostics = new StockageMemoireIndexParId<DiagnosticEntite>();

    ajouterDiagnostic(diagnostic: DiagnosticEntite): void {
        this.stockageDiagnostics.save(diagnostic);
    }

    rechercherOuCreerDiagnostic(
        idTerritoire: string,
        nomTerritoire: string,
        categorieTerritoire: string
    ): DiagnosticEntite {
        let diagnostic = this.rechercher(idTerritoire);
        if (!diagnostic) {
            diagnostic = new DiagnosticEntite(idTerritoire, nomTerritoire, categorieTerritoire);
            this.ajouterDiagnostic(diagnostic);
        }
        return diagnostic;
    }

    rechercherTous(): Array<DiagnosticEntite> {
        return this.stockageDiagnostics.findAll();
    }

    rechercherTousParCategorie(categorieTerritoire: string): Array<DiagnosticEntite> {
        return this.stockageDiagnostics.findAll().filter((d) => d.categorieTerritoire === categorieTerritoire);
    }

    rechercher(idTerritoire: string): DiagnosticEntite | undefined {
        return this.stockageDiagnostics.findById(idTerritoire);
    }
}
