import { Application, Request, Response } from 'express';
import { ServicesTerritoires } from './domaines/ServicesTerritoires';
import {
    Commune,
    Departement,
    Epci,
    Pays,
    Region,
    RegroupementCommunes,
    Territoire
} from './domaines/entitesTerritoires';
import { ErreurRessourceNonTrouvee } from '../communs/erreurs';
import {
    Territoire as TerritoireApi,
    TerritoireSynthese,
    TerritoireSynthese as TerritoireSyntheseApi
} from '../../src_generated/api/model/models';
import { Etat, verifierEtatApi } from '../communs/apiUtils';

export class ApiTerritoires {
    private stockageTerritoires: ServicesTerritoires;
    private expressApp: Application;
    public etat = Etat.INIT_EN_COURS;

    constructor(expressApp: Application, stockageTerritoires: ServicesTerritoires) {
        this.expressApp = expressApp;
        this.stockageTerritoires = stockageTerritoires;
    }

    configurerRoutes(): void {
        this.expressApp.get('/crater/api/territoires', this.construireReponseGetTerritoires);
        this.expressApp.get('/crater/api/territoires/:idTerritoire', this.construireReponseGetTerritoireParId);
    }

    private construireReponseGetTerritoires = (req: Request, res: Response) => {
        verifierEtatApi(this.etat);
        res.json(
            this.rechercherSynthesesTerritoiresParNomOuCodePostal(
                req.query.critere ? req.query.critere.toString() : '',
                req.query.nbMaxResultats ? req.query.nbMaxResultats.toString() : ''
            )
        );
    };

    private construireReponseGetTerritoireParId = (req: Request, res: Response) => {
        verifierEtatApi(this.etat);
        res.json(this.rechercherTerritoireParId(req.params.idTerritoire));
    };

    private rechercherSynthesesTerritoiresParNomOuCodePostal(
        critere: string,
        nbMaxResultats: string
    ): TerritoireSyntheseApi[] {
        let nbMaxResultatAsNumber = parseInt(nbMaxResultats) ? parseInt(nbMaxResultats) : 10;
        let territoires = this.stockageTerritoires.rechercherTerritoireParCritere(critere, nbMaxResultatAsNumber);
        let listeTerritoiresSynthese = territoires.map(this.creerObjetTerritoireSynthese);
        return this.ajouterTerritoireSuggere(listeTerritoiresSynthese, nbMaxResultatAsNumber);
    }

    //  TODO : beaucoup de logique métier ici, il faudrait rappatrier dans le service stockageTerritoires et tester l'ensemble
    // Mais implique un peu de refacto, a faire dans un 2eme temps donc
    private ajouterTerritoireSuggere(
        listeTerritoiresSynthese: TerritoireSynthese[],
        nbMaxResultats: number
    ): TerritoireSynthese[] {
        let listeAvecTerritoireSuggere = listeTerritoiresSynthese;
        if (listeTerritoiresSynthese.length > 0 && listeTerritoiresSynthese[0].categorie === 'COMMUNE') {
            let commune = this.stockageTerritoires.rechercher(listeTerritoiresSynthese[0].id) as Commune;
            let epciAppartenance = this.stockageTerritoires.rechercher(commune.idEpci) as Epci;
            let territoireSuggere = this.creerObjetTerritoireSynthese(epciAppartenance);
            territoireSuggere.estSuggere = true;
            territoireSuggere.libelleSecondaire = `Intercommunalité de ${commune.nom}`;
            listeAvecTerritoireSuggere = listeTerritoiresSynthese.filter((t) => t.id !== territoireSuggere.id);
            listeAvecTerritoireSuggere.unshift(territoireSuggere);
            listeAvecTerritoireSuggere = listeAvecTerritoireSuggere.slice(0, nbMaxResultats);
        }
        return listeAvecTerritoireSuggere;
    }

    private creerObjetTerritoireSynthese(territoire: Territoire): TerritoireSyntheseApi {
        let territoireSynthese = <TerritoireSyntheseApi>{
            id: territoire.id,
            idParcel: territoire.idParcel,
            nom: territoire.nom,
            categorie: <any>territoire.categorie,
            libellePrincipal: territoire.libelle,
            libelleSecondaire: territoire.libelleSecondaire
        };
        if (territoire instanceof RegroupementCommunes) {
            territoireSynthese.sousCategorie = (<RegroupementCommunes>territoire).sousCategorie;
        }
        return territoireSynthese;
    }

    private rechercherTerritoireParId(idTerritoire: string): TerritoireApi {
        const territoire = this.stockageTerritoires.rechercher(idTerritoire);
        if (!territoire) {
            throw new ErreurRessourceNonTrouvee('Territoire non trouvé (idTerritoire=' + idTerritoire + ')');
        }
        return this.creerObjetTerritoireApi(territoire);
    }

    private creerObjetTerritoireApi(territoire: Territoire): TerritoireApi {
        let territoireApi = {
            idTerritoire: territoire.id,
            nomTerritoire: territoire.nom,
            categorieTerritoire: <any>territoire.categorie,
            idParcel: territoire.idParcel
        } as TerritoireApi;

        if (territoire instanceof Region) {
            this.ajouterPays(territoire.idPays, territoireApi);
        }
        if (territoire instanceof Departement) {
            this.ajouterPays(territoire.idPays, territoireApi);
            this.ajouterRegion(territoire.idRegion, territoireApi);
        }
        if (territoire instanceof RegroupementCommunes) {
            territoireApi.sousCategorieTerritoire = (<RegroupementCommunes>territoire).sousCategorie;
            this.ajouterPays(territoire.idPays, territoireApi);
            this.ajouterRegion(territoire.idRegion, territoireApi);
            this.ajouterDepartement(territoire.idDepartement, territoireApi);
        }
        if (territoire instanceof Commune) {
            this.ajouterPays(territoire.idPays, territoireApi);
            this.ajouterRegion(territoire.idRegion, territoireApi);
            this.ajouterDepartement(territoire.idDepartement, territoireApi);
            this.ajouterEpci(territoire.idEpci, territoireApi);
        }
        return territoireApi;
    }

    private ajouterEpci(idTerritoire: string, territoireResultat: TerritoireApi): void {
        const territoire = this.stockageTerritoires.rechercher(idTerritoire);
        if (territoire && territoire instanceof Epci) {
            const epci = territoire as Epci;
            territoireResultat.epci = {
                id: epci.id,
                nom: epci.nom
            };
        }
    }

    private ajouterDepartement(idTerritoire: string, territoireResultat: TerritoireApi): void {
        const territoire = this.stockageTerritoires.rechercher(idTerritoire);
        if (territoire && territoire instanceof Departement) {
            const departement = territoire as Departement;
            territoireResultat.departement = {
                id: departement.id,
                nom: departement.nom
            };
        }
    }

    private ajouterRegion(idTerritoire: string, territoireResultat: TerritoireApi): void {
        const territoire = this.stockageTerritoires.rechercher(idTerritoire);
        if (territoire && territoire instanceof Region) {
            const region = territoire as Region;
            territoireResultat.region = {
                id: region.id,
                nom: region.nom
            };
        }
    }

    private ajouterPays(idTerritoire: string, territoireResultat: TerritoireApi): void {
        const territoire = this.stockageTerritoires.rechercher(idTerritoire);
        if (territoire && territoire instanceof Pays) {
            const pays = territoire as Pays;
            territoireResultat.pays = {
                id: pays.id,
                nom: pays.nom
            };
        }
    }
}
