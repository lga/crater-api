## INSTALL INITIALE DU SERVEUR - à ne faire qu'une seule fois

# APACHE
sudo apt-get install apache2
# Activer ssl & proxy pour apache
sudo a2enmod ssl
sudo a2ensite default-ssl
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo systemctl reload apache2

#installer cerbot qui permet d'installer facilement des certificats lets encrypt
sudo apt update
sudo apt install certbot

# installer nvm pour gérer facilement les versions de node
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

# activer NVM
. ~/.bashrc

# installer la version de node
nvm install 16

# utiliser node 16
nvm use 16

#installer pm2 (gestion et monitoring des process en prod)
npm install pm2 -g

# configurer pm2 pour qu'il redémarre a chaque fois
NODE_SCRIPT_PATH=$(which node)
NODE_BIN_DIR="$(dirname "$NODE_PATH")"
sudo env PATH=$PATH:$NODE_BIN_DIR $NODE_BIN_DIR/pm2 startup systemd -u ubuntu --hp /home/ubuntu
