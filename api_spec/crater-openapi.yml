openapi: 3.0.2
info:
  version: '1.0'
  title: CRATER API
servers:
  - url: https://crater.resiliencealimentaire.org/crater/api
    description: Serveur CRATer API
paths:
  /territoires:
    get:
      summary: Rechercher des territoires par nom (ou code postal pour les communes)
      tags:
        - API Territoires
      parameters:
        - name: critere
          in: query
          required: true
          description: "chaine de caractère à rechercher dans le nom du territoire, ou le code postal pour les communes. Peut contenir un nom exact du territoire ou une partie seulement. Par défaut la recherche s'effectue sur toutes les catégories de territoires. Si le critere de recherche contient le nom d'une ou plusieurs categories, la recherche s'effectue uniquement sur ces catégories."
          schema:
            type: string
        - name: nbMaxResultats
          in: query
          description: "Nombre maximum d'éléments retournés dans la liste des résultats (valeur par défaut : 10)"
          schema:
            type: string
      responses:
        '200':
          $ref: '#/components/responses/TableauTerritoires'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
        '503':
          $ref: '#/components/responses/503'
  /territoires/{idTerritoire}:
    get:
      summary: "Récupérer les informations détaillées d'un territoire par son identifiant."
      tags:
        - API Territoires
      parameters:
        - name: idTerritoire
          required: true
          in: path
          schema:
            type: string
      responses:
        '200':
          $ref: '#/components/responses/Territoire'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
        '503':
          $ref: '#/components/responses/503'
  /diagnostics/{idTerritoire}:
    get:
      summary: Récupérer le diagnotic du système alimentaire pour un territoire
      tags:
        - API Diagnostics
      parameters:
        - $ref: '#/components/parameters/idTerritoireDiagnostic'
      responses:
        '200':
          $ref: '#/components/responses/Diagnostic'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
        '503':
          $ref: '#/components/responses/503'
  /diagnostics/csv/{idTerritoire}:
    get:
      summary: Récupérer le diagnotic du système alimentaire pour un territoire au format csv
      tags:
        - API Diagnostics
      parameters:
        - $ref: '#/components/parameters/idTerritoireDiagnostic'
      responses:
        '200':
          description: Résultat du diagnostic pour ce territoire, au format csv
          content:
            text/csv:
              schema:
                type: array
                items:
                  type: string
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
        '503':
          $ref: '#/components/responses/503'
  /indicateurs/{nomIndicateur}:
    get:
      summary: Récupérer les valeurs d'un indicateur ou groupe d'indicateurs pour un ensemble de territoires
      tags:
        - API Diagnostics
      parameters:
        - name: nomIndicateur
          required: true
          in: path
          description: Nom de l'indicateur à récupérer. Correspond à un chemin dans l'objet résultat du GET /diagnostics, et peut cibler un champ simple de type number ou string, un champ de type objet, ou un champ string ou number contenu dans un tableau. Par exemple 'population', ou 'politiqueFonciere.indicateurGlobal'
          schema:
            type: string
        - name: categorieTerritoire
          in: query
          description: Catégorie de territoire pour lesquels récupérer les indicateurs. Valeurs valides = COMMUNE, EPCI, DEPARTEMENT, REGION ou PAYS
          schema:
            type: string
        - name: idsDepartements
          in: query
          description: Limiter le résultat à 1 ou plusieurs départements. Si categorieTerritoire=COMMUNE ce paramètre est obligatoire, sinon il est ignoré. Les identifiants de départements sont séparés par le caractère '|'. Exemple de valeurs pour le paramètre 'gers' ou 'gers|landes'
          schema:
            type: string
        - name: annee
          in: query
          description: Permet de filtrer sur une année en particulier. Utilisable uniquement dans le cas ou l'indicateur ciblé par nomIndicateur est une valeur string ou number appartenant à un tableau d'objets, et qu'un attribut "annee" est défini pour ces objets
          schema:
            type: string
      responses:
        '200':
          $ref: '#/components/responses/TableauIndicateurs'
        '400':
          $ref: '#/components/responses/400'
        '404':
          $ref: '#/components/responses/404'
        '500':
          $ref: '#/components/responses/500'
        '503':
          $ref: '#/components/responses/503'
components:
  schemas:
    TerritoireSynthese:
      type: object
      properties:
        id:
          type: string
          description: Code insee du territoire
          example: C-14118
        nom:
          type: string
          example: Caen
        categorie:
          type: string
          enum: [ PAYS, REGION, DEPARTEMENT, REGROUPEMENT_COMMUNES, EPCI, COMMUNE ]
        sousCategorie:
          description: "Information présente uniquement pour les EPCIs et REGROUPEMENT_COMMUNES. Permet de préciser le type d'EPCI (ex : METROPOLE, COMMUNAUTE_COMMUNE,...) ou de regroupement de communes (ex : PNR, SCOR, CARRE_METROPOLITAIN,...)."
          type: string
        libellePrincipal:
          type: string
          example: Caen (14100)
        libelleSecondaire:
          type: string
          example: Intercommunalité de Caen
        estSuggere:
          type: boolean
        idParcel:
          type: string
          description: id du territoire dans l'application Parcel
          example: 4522
      required:
        - id
        - nom
        - categorie
        - libellePrincipal
        - libelleSecondaire
    Territoire:
      type: object
      properties:
        idTerritoire:
          type: string
          description: identifiant du territoire
          example: C-14118
        categorieTerritoire:
          type: string
          enum: [ PAYS, REGION, DEPARTEMENT, REGROUPEMENT_COMMUNES, EPCI, COMMUNE ]
        sousCategorieTerritoire:
          description: "Information présente uniquement pour les EPCIs et REGROUPEMENT_COMMUNES. Permet de préciser le type d'EPCI (ex : METROPOLE, COMMUNAUTE_COMMUNE,...) ou de regroupement de communes (ex : PNR, SCOR, CARRE_METROPOLITAIN,...)."
          type: string
        nomTerritoire:
          type: string
          example: Caen
        idParcel:
          type: string
        pays:
          $ref: '#/components/schemas/Pays'
        region:
          $ref: '#/components/schemas/Region'
        departement:
          $ref: '#/components/schemas/Departement'
        epci:
          $ref: '#/components/schemas/Epci'
      required:
        - idTerritoire
        - categorieTerritoire
        - nomTerritoire
    Pays:
      type: object
      properties:
        id:
          type: string
        nom:
          type: string
      required:
        - id
        - nom
    Region:
      type: object
      properties:
        id:
          type: string
        nom:
          type: string
      required:
        - id
        - nom
    Departement:
      type: object
      properties:
        id:
          type: string
        nom:
          type: string
      required:
        - id
        - nom
    Epci:
      type: object
      properties:
        id:
          type: string
        nom:
          type: string
      required:
        - id
        - nom
    Diagnostic:
      type: object
      properties:
        idTerritoire:
          type: string
          description: identifiant de territoire (commune, epci, département, etc...)
        nomTerritoire:
          type: string
        population:
          type: number
          nullable: true
        superficieHa:
          type: number
          nullable: true
        idTerritoireParcel:
          type: string
          description: Identifiant du territoire dans l'application PARCEL
        nbCommunes:
          type: number
          nullable: true
        nbCommunesParOtex:
          type: number
          nullable: true
          $ref: '#/components/schemas/NbCommunesParOtex'
        surfaceAgricoleUtile:
          $ref: '#/components/schemas/SurfaceAgricoleUtile'
        politiqueFonciere:
          $ref: '#/components/schemas/PolitiqueFonciere'
        consommation:
          $ref: '#/components/schemas/Consommation'
        populationAgricole:
          $ref: '#/components/schemas/PopulationAgricole'
        intrants:
          $ref: '#/components/schemas/Intrants'
        pratiquesAgricoles:
          $ref: '#/components/schemas/PratiquesAgricoles'
        productionsBesoins:
          $ref: '#/components/schemas/ProductionsBesoins'
        production:
          $ref: '#/components/schemas/Production'
        proximiteCommerces:
          $ref: '#/components/schemas/ProximiteCommerces'
      required:
        - idTerritoire
        - nomTerritoire
        - population
        - superficieHa
        - idTerritoireParcel
        - nbCommunes
        - nbCommunesParOtex
        - surfaceAgricoleUtile
        - politiqueFonciere
        - consommation
        - populationAgricole
        - intrants
        - pratiquesAgricoles
        - productionsBesoins
        - production
        - proximiteCommerces
    NbCommunesParOtex:
      type: object
      properties:
        grandesCultures:
          type: number
          nullable: false
        maraichageHorticulture:
          type: number
          nullable: false
        viticulture:
          type: number
          nullable: false
        fruits:
          type: number
          nullable: false
        bovinLait:
          type: number
          nullable: false
        bovinViande:
          type: number
          nullable: false
        bovinMixte:
          type: number
          nullable: false
        ovinsCaprinsAutresHerbivores:
          type: number
          nullable: false
        porcinsVolailles:
          type: number
          nullable: false
        polyculturePolyelevage:
          type: number
          nullable: false
        nonClassees:
          type: number
          nullable: false
        sansExploitations:
          type: number
          nullable: false
        nonRenseigne:
          type: number
          nullable: false
      required:
        - grandesCultures
        - maraichageHorticulture
        - viticulture
        - fruits
        - bovinLait
        - bovinViande
        - bovinMixte
        - ovinsCaprinsAutresHerbivores
        - porcinsVolailles
        - polyculturePolyelevage
        - nonClassees
        - sansExploitations
        - nonRenseigne
    SurfaceAgricoleUtile:
      type: object
      properties:
        sauTotaleHa:
          type: number
          nullable: true
        sauProductiveHa:
          type: number
          nullable: true
          description: Surface des cultures productives (cad excluant certains types de cultures comme les jachères, les estives, ...)
        sauPeuProductiveHa:
          type: number
          nullable: true
          description: Surface des cultures peu productives (telles que les jachères, les estives, ...)
        sauBioHa:
          type: number
          nullable: true
        sauParGroupeCulture:
          type: array
          items:
            $ref: '#/components/schemas/SauParGroupeCulture'
      required:
        - sauTotaleHa
        - sauProductiveHa
        - sauPeuProductiveHa
        - sauBioHa
        - sauParGroupeCulture
    PolitiqueFonciere:
      type: object
      properties:
        note:
          type: number
          nullable: true
        artificialisation5ansHa:
          type: number
          nullable: true
        evolutionMenagesEmplois5ans:
          type: number
          nullable: true
        sauParHabitantM2:
          description: Surface Agricole Utile par habitant en m2
          type: number
          nullable: true
        rythmeArtificialisationSauPourcent:
          type: number
          nullable: true
        evaluationPolitiqueAmenagement:
          type: string
          nullable: true
        partLogementsVacants2013Pourcent:
          type: number
          nullable: true
        partLogementsVacants2018Pourcent:
          type: number
          nullable: true
      required:
        - note
        - artificialisation5ansHa
        - evolutionMenagesEmplois5ans
        - sauParHabitantM2
        - rythmeArtificialisationSauPourcent
        - evaluationPolitiqueAmenagement
        - partLogementsVacants2013Pourcent
        - partLogementsVacants2018Pourcent
    PopulationAgricole:
      type: object
      properties:
        note:
          type: number
          nullable: true
        populationAgricole1988:
          type: number
          nullable: true
        populationAgricole2010:
          type: number
          nullable: true
        partPopulationAgricole2010Pourcent:
          type: number
          nullable: true
        partPopulationAgricole1988Pourcent:
          type: number
          nullable: true
        nbExploitations1988:
          type: number
          nullable: true
        nbExploitations2010:
          type: number
          nullable: true
        sauHa1988:
          type: number
          nullable: true
        sauHa2010:
          type: number
          nullable: true
        sauMoyenneParExploitationHa1988:
          type: number
          nullable: true
        sauMoyenneParExploitationHa2010:
          type: number
          nullable: true
        nbExploitationsParClassesAgesChefExploitation:
          $ref: '#/components/schemas/ValeursParClassesAges'
        nbExploitationsParClassesSuperficies:
          $ref: '#/components/schemas/ValeursParClassesSuperficies'
        sauHaParClassesSuperficies:
          $ref: '#/components/schemas/ValeursParClassesSuperficies'
      required:
        - note
        - populationAgricole1988
        - populationAgricole2010
        - partPopulationAgricole1988Pourcent
        - partPopulationAgricole2010Pourcent
        - nbExploitations1988
        - nbExploitations2010
        - sauHa1988
        - sauHa2010
        - sauMoyenneParExploitationHa1988
        - sauMoyenneParExploitationHa2010
        - nbExploitationsParClassesAgesChefExploitation
        - nbExploitationsParClassesSuperficies
        - sauHaParClassesSuperficies
    ValeursParClassesAges:
      type: object
      properties:
        moins_40_ans:
          type: number
          nullable: true
        de_40_a_49_ans:
          type: number
          nullable: true
        de_50_a_59_ans:
          type: number
          nullable: true
        plus_60_ans:
          type: number
          nullable: true
      required:
        - moins_40_ans
        - de_40_a_49_ans
        - de_50_a_59_ans
        - plus_60_ans
    ValeursParClassesSuperficies:
      type: object
      properties:
        moins_20_ha:
          type: number
          nullable: true
        de_20_a_50_ha:
          type: number
          nullable: true
        de_50_a_100_ha:
          type: number
          nullable: true
        de_100_a_200_ha:
          type: number
          nullable: true
        plus_200_ha:
          type: number
          nullable: true
      required:
        - moins_20_ha
        - de_20_a_50_ha
        - de_50_a_100_ha
        - de_100_a_200_ha
        - plus_200_ha
    Intrants:
      type: object
      properties:
        note:
          type: number
          nullable: true
        noduNormalise:
          type: number
          nullable: true
        pesticides:
          $ref: '#/components/schemas/Pesticides'
      required:
        - note
        - noduNormalise
        - pesticides
    Pesticides:
      type: object
      properties:
        indicateursMoyennesTriennales:
          type: array
          items:
            $ref: '#/components/schemas/IndicateursPesticidesAnneeN'
      required:
        - indicateursMoyennesTriennales
    IndicateursPesticidesAnneeN:
      type: object
      properties:
        annee:
          type: number
          nullable: false
        quantiteSubstanceSansDUKg:
          type: number
          nullable: true
        quantiteSubstanceAvecDUKg:
          type: number
          nullable: true
        noduHa:
          type: number
          nullable: true
        noduNormalise:
          type: number
          nullable: true
      required:
        - annee
        - quantiteSubstanceSansDUKg
        - quantiteSubstanceAvecDUKg
        - noduHa
        - noduNormalise
    PratiquesAgricoles:
      type: object
      properties:
        partSauBioPourcent:
          type: number
          nullable: true
        indicateurHvn:
          $ref: '#/components/schemas/IndicateurHvn'
      required:
        - indicateurHvn
        - partSauBioPourcent
    Consommation:
      type: object
      properties:
        tauxPauvrete60Pourcent:
          type: number
          nullable: true
      required:
        - tauxPauvrete60Pourcent
    IndicateurHvn:
      type: object
      properties:
        indice1:
          type: number
          nullable: true
          description: note sur 10 mesurant la diversité des assolements
        indice2:
          type: number
          nullable: true
          description: note sur 10 mesurant le caractère durable des pratiques agricoles
        indice3:
          type: number
          nullable: true
          description: note sur 10 mesurant présence d’infrastructures d’intérêt écologique (haies, bosquets, pré-vergers, etc.)
        indiceTotal:
          type: number
          nullable: true
          description: note sur 30, cumul des indices 1, 2 et 3
      required:
        - indice1
        - indice2
        - indice3
        - indiceTotal
    ProductionsBesoins:
      type: object
      properties:
        besoinsAssietteActuelle:
          $ref: '#/components/schemas/BesoinsAssiette'
        besoinsAssietteDemitarienne:
          $ref: '#/components/schemas/BesoinsAssiette'  
      required:
         - besoinsAssietteActuelle
         - besoinsAssietteDemitarienne
    BesoinsAssiette:
      type: object
      properties:
        besoinsHa:
          type: number
          nullable: true
        tauxAdequationBrutPourcent:
          type: number
          nullable: true
        tauxAdequationMoyenPonderePourcent:
          type: number
          nullable: true     
        besoinsParGroupeCulture:
          type: array
          items:
            $ref: '#/components/schemas/BesoinsParGroupeCulture'
      required:
         - besoinsHa
         - tauxAdequationBrutPourcent
         - tauxAdequationMoyenPonderePourcent
         - besoinsParGroupeCulture
    BesoinsParGroupeCulture:
      type: object
      properties:
        codeGroupeCulture:
          type: string
        nomGroupeCulture:
          type: string
        besoinsHa:
          type: number
          nullable: true
        tauxAdequationBrutPourcent:
          type: number
          nullable: true
        besoinsParCulture:
          type: array
          items:
            $ref: '#/components/schemas/BesoinsParCulture'
      required:
        - codeGroupeCulture
        - nomGroupeCulture
        - besoinsHa
        - tauxAdequationBrutPourcent
        - besoinsParCulture
    BesoinsParCulture:
      type: object
      properties:
        codeCulture:
          type: string
        nomCulture:
          type: string
        alimentationAnimale:
          type: boolean
        besoinsHa:
          type: number
          nullable: true
      required:
        - codeCulture
        - nomCulture
        - alimentationAnimale
        - besoinsHa
    SauParGroupeCulture:
      type: object
      properties:
        codeGroupeCulture:
          type: string
        nomGroupeCulture:
          type: string
        sauHa:
          type: number
          nullable: true
        sauParCulture:
          type: array
          items:
            $ref: '#/components/schemas/SauParCulture'
      required:
        - codeGroupeCulture
        - nomGroupeCulture
        - sauHa
        - sauParCulture
    SauParCulture:
      type: object
      properties:
        codeCulture:
          type: string
        nomCulture:
          type: string
        sauHa:
          type: number
          nullable: true
      required:
        - codeCulture
        - nomCulture
        - sauHa
    Production:
      type: object
      properties:
        note:
          type: number
          nullable: true
        tauxAdequationMoyenPonderePourcent:
          type: number
          nullable: true
        partSauBioPourcent:
          type: number
          nullable: true
        indiceHvn:
          type: number
          nullable: true
        noteTauxAdequationMoyenPondere:
          type: number
          nullable: true
        notePartSauBio:
          type: number
          nullable: true
        noteHvn:
          type: number
          nullable: true
      required:
        - note
        - tauxAdequationMoyenPonderePourcent
        - partSauBioPourcent
        - indiceHvn
        - noteTauxAdequationMoyenPondere
        - notePartSauBio
        - noteHvn
    ProximiteCommerces:
      type: object
      properties:
        note:
          type: number
          nullable: true
        partPopulationDependanteVoiturePourcent:
          type: number
          nullable: true
        partTerritoireDependantVoiturePourcent:
          type: number
          nullable: true
        indicateursParTypesCommerces:
          type: array
          items:
            $ref: '#/components/schemas/IndicateursParTypeCommerce'
      required:
        - note
        - partPopulationDependanteVoiturePourcent
        - partTerritoireDependantVoiturePourcent
        - indicateursParTypesCommerces
    IndicateursParTypeCommerce:
      type: object
      properties:
        typeCommerce:
          type: string
          nullable: false
        distancePlusProcheCommerceMetres:
          type: number
          nullable: true
        partPopulationAccesAPiedPourcent:
          type: number
          nullable: true
        partPopulationAccesAVeloPourcent:
          type: number
          nullable: true
        partPopulationDependanteVoiturePourcent:
          type: number
          nullable: true
      required:
        - typeCommerce
        - distancePlusProcheCommerceMetres
        - partPopulationAccesAPiedPourcent
        - partPopulationAccesAVeloPourcent
        - partPopulationDependanteVoiturePourcent
    Indicateur:
      properties:
        idTerritoire:
          type: string
        valeur:
          $ref: '#/components/schemas/ValeurIndicateur'
    ValeurIndicateur:
      description: Un indicateur ou groupe d'indicateurs disponible dans le diagnostic. Le contenu de cet élément dépend de la valeur de nomIndicateur passée en paramètre. Il peut etre de type number, string, boolean ou objet
    RestError:
      type: object
      properties:
        message:
          type: string
        description:
          type: string
  parameters:
    idTerritoireDiagnostic:
      name: idTerritoire
      required: true
      in: path
      schema:
        type: string
  responses:
    Diagnostic:
      description: Résultat du diagnostic pour ce territoire
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Diagnostic'
    Territoire:
      description: Territoire
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Territoire'
    TableauTerritoires:
      description: Liste des territoires correspondant à la recherche
      content:
        application/json:
          schema:
            type: array
            items:
              $ref: '#/components/schemas/TerritoireSynthese'
    TableauIndicateurs:
      description: Liste d'indicateurs
      content:
        application/json:
          schema:
            type: array
            items:
              $ref: '#/components/schemas/Indicateur'
    400:
      description: Requête incorrecte
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/RestError"
    404:
      description: Ressource introuvable
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/RestError"
    500:
      description: 500 Service indisponible, erreur interne
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/RestError"
    503:
      description: 503 Service temporairement indisponible, réessayer dans quelques instants
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/RestError"