# Crater API

## Installer l'environnement de dev

Pré-requis :

1. Installer [nodejs](https://nodejs.org/en/download/) (version recommandée 16.13.2)
2. Installer un [runtime java](https://openjdk.java.net/) si pas déjà présent

Puis :

1. Cloner le dépôt.
2. Dans le répertoire du dépot faire un `npm install`

## Tester

Pour les tests unitaires, lancer `npm run unit-test`

Pour l'ensemble des tests (unitaires + intégration), lancer `npm run test`. Attention, pour fonctionner, cet étape
nécessite de disposer des données `crater-data-resultat` en local (voir paragraphe suivant)

## Exécuter l'application en local

Pré-requis : disposer des données `crater-data-resultat` en local

1. Clôner le dépôt `crater-data-resultats` (`https://framagit.org/lga/crater-data-resultats.git`) dans `../crater-data-resultats` par rapport à `crater-api` (même répertoire
   parent)
2. Copier les données `crater-data-resultats` dans le projet local `./dist/crater-data-resultats` en
   lancant `npm run localpackage:copy-crater-data-resultat`

Puis :

1. Builder l'application avec `npm run package`
2. Lancer l'application en local : `cd ./dist/packaged && node server.js`
   L'api est accessible sur [localhost:3000](http://localhost:3000), par exemple à cette
   adresse http://localhost:3000/crater/api/territoires/toulouse

## Livrer

L'application est déployée automatiquement sur les 2 environnements de développement et de production, via des pipelines
gitlab définis dans le fichier [.gitlab-ci.yml](.gitlab-ci.yml)

1- Lors de chaque commit sur la branche develop, le code correspondant est poussé sur l'hébergeur o2switch. L'avancement
et le résultat des pipelines est visible ici https://framagit.org/lga/crater-ui/pipelines

Une fois le pipeline terminé, pour que les modifications soient prises en compte, il est nécessaire de se connecter sur
l'outils d'aministration o2switch pour remédarrer l'application nodejs.

Pour cela :

-   Connexion à cette adresse https://macaroni.o2switch.net:2083/
-   login/mdp du compte de l'association Les Greniers d'Abondance
-   Accéder à l'outil "Setup Node.js App", puis redémarrer l'application "api.resiliencealimentaire.org/dev/crater/api"

Il est ensuite possible de tester à l'api à cette adresse https://api.resiliencealimentaire.org/dev/crater/api/territoires/toulouse ou https://dev.resiliencealimentaire.org/dev/crater/api/diagnostics/toulouse

2- Même fonctionnement sur la branche master, ou les mises à jour sont livrées sur l'environnement de production : https://api.resiliencealimentaire.org/crater/api
Dans ce cas, on peut tester avec cette url : https://api.resiliencealimentaire.org/crater/api/territoires/toulouse

Rq : 
-  La branche master est protégée, il n'est pas possible de commiter directement dessus, il faut passer par une merge request gitlab
-  Lors de son exécution, crater-api utilise les données de crater-data-resultats. Ces données sont récupérées lors de l'exécution des pipelines gitlab, le git clone est fait automatiquement. 