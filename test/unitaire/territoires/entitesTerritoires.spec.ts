import { Epci, RegroupementCommunes } from '../../../src/territoires/domaines/entitesTerritoires';

describe('Test unitaire des entités territoires', () => {
    it("Calculer le libellé d'un regroupement de communes", () => {
        const pnr = new RegroupementCommunes(
            'pnr10',
            'PNR Fictif',
            'PARC_NATUREL_REGIONAL',
            'departement1',
            'region1',
            'france'
        );
        expect(pnr.libelle).toEqual('PNR Fictif');

        const bassin_de_vie = new RegroupementCommunes(
            'bassin-de-vie-1',
            'Bassin de vie Fictif',
            'BASSIN_DE_VIE_2012',
            'departement1',
            'region1',
            'france'
        );
        expect(bassin_de_vie.libelle).toEqual('Bassin de vie Fictif');
    });

    it("Calculer le libellé d'un epci", () => {
        const epci = new Epci('epci1', 'EPCI Fictif', 'METROPOLE', 'departement1', 'region1', 'france');
        expect(epci.libelle).toEqual('EPCI Fictif');
    });
});
