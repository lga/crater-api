import {
    ConsommationEntite,
    IndicateursPesticidesEntite,
    IndicateursProximiteCommercesEntite
} from '../../../src/diagnostics/domaines/entitesDiagnostics';

describe('Test unitaire des entités diagnostic', () => {
    it("Test de l'ajout de valeur pour une année donnée dans les indicateurs pesticides", () => {
        const indicateurs = new IndicateursPesticidesEntite();

        indicateurs.ajouterValeursAnneeN(2018, 108, 208, 158, 8);

        expect(indicateurs.toutesAnnees).toEqual([
            {
                noduHa: 158,
                noduNormalise: 8,
                annee: 2018,
                quantiteSubstanceAvecDUKg: 208,
                quantiteSubstanceSansDUKg: 108
            }
        ]);

        // ajout pour une année antérieure pour vérifier le bon ordonnancement
        indicateurs.ajouterValeursAnneeN(2017, 177, 277, 157, 7);

        expect(indicateurs.toutesAnnees).toEqual([
            {
                noduHa: 157,
                noduNormalise: 7,
                annee: 2017,
                quantiteSubstanceAvecDUKg: 277,
                quantiteSubstanceSansDUKg: 177
            },
            {
                noduHa: 158,
                noduNormalise: 8,
                annee: 2018,
                quantiteSubstanceAvecDUKg: 208,
                quantiteSubstanceSansDUKg: 108
            }
        ]);
    });

    it("Test de l'ajout de valeur pour un type de commerce donné dans les indicateurs de proximité aux commerces", () => {
        const indicateurs = new IndicateursProximiteCommercesEntite();

        indicateurs.ajouterValeursPourUnTypeDeCommerce('B', 2.8, 10, 20, 55, 56);

        expect(indicateurs.tousTypesCommerces).toEqual([
            {
                distancePlusProcheCommerceMetres: 2.8,
                partPopulationAccesAPiedPourcent: 10,
                partPopulationAccesAVeloPourcent: 20,
                partPopulationDependanteVoiturePourcent: 55,
                partTerritoireDependantVoiturePourcent: 56,
                typeCommerce: 'B'
            }
        ]);

        indicateurs.ajouterValeursPourUnTypeDeCommerce('A', 3.8, 15, 25, 60, 65);

        expect(indicateurs.tousTypesCommerces).toEqual([
            {
                distancePlusProcheCommerceMetres: 2.8,
                partPopulationAccesAPiedPourcent: 10,
                partPopulationAccesAVeloPourcent: 20,
                partPopulationDependanteVoiturePourcent: 55,
                partTerritoireDependantVoiturePourcent: 56,
                typeCommerce: 'B'
            },
            {
                distancePlusProcheCommerceMetres: 3.8,
                partPopulationAccesAPiedPourcent: 15,
                partPopulationAccesAVeloPourcent: 25,
                partPopulationDependanteVoiturePourcent: 60,
                partTerritoireDependantVoiturePourcent: 65,
                typeCommerce: 'A'
            }
        ]);
    });
});
