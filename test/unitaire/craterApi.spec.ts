import CraterApp from '../../src/CraterApp';
import { Request, Response } from 'express';
import { supprimerNPremieresLignes, verifierEgaliteAvecFichierTexte } from '../test_outils';
import { SAUT_LIGNE } from '../../src/communs/apiUtils';
import express = require('express');
import request = require('supertest');

const CHEMIN_DONNEES_TESTS_UNITAIRES = './test/unitaire/data/';

jest.setTimeout(10000);

describe('Tests unitaire API CRATer sur les données de test', () => {
    let craterApp: CraterApp;
    let expressApp: express.Application;

    beforeAll(async function () {
        expressApp = express();
        craterApp = new CraterApp(expressApp, `${CHEMIN_DONNEES_TESTS_UNITAIRES}/input`);
        await craterApp.init();
    });

    it('Test API GET /territoires recherche par nom', (done) => {
        request(expressApp)
            .get('/crater/api/territoires?critere=pari')
            .expect('Content-Type', /json/)
            .expect(
                200,
                [
                    {
                        id: 'paris-departement',
                        idParcel: null,
                        nom: 'Paris',
                        categorie: 'DEPARTEMENT',
                        libellePrincipal: 'Paris',
                        libelleSecondaire: 'Département'
                    },
                    {
                        id: 'paris-commune',
                        idParcel: null,
                        nom: 'Paris',
                        categorie: 'COMMUNE',
                        libellePrincipal: 'Paris (75010, 75011)',
                        libelleSecondaire: 'Commune'
                    },
                    {
                        id: 'metropole-du-grand-paris',
                        idParcel: null,
                        nom: 'Métropole du Grand Paris',
                        categorie: 'EPCI',
                        libellePrincipal: 'Métropole du Grand Paris',
                        libelleSecondaire: 'Intercommunalité',
                        sousCategorie: 'METROPOLE'
                    }
                ],
                done
            );
    });

    it('Test API GET /territoires recherche par nom avec nbMaxResultats', (done) => {
        request(expressApp)
            .get('/crater/api/territoires?critere=pari&nbMaxResultats=1')
            .expect('Content-Type', /json/)
            .expect(
                200,
                [
                    {
                        id: 'paris-departement',
                        idParcel: null,
                        nom: 'Paris',
                        categorie: 'DEPARTEMENT',
                        libellePrincipal: 'Paris',
                        libelleSecondaire: 'Département'
                    }
                ],
                done
            );
    });

    it('Test API GET /territoires recherche par code postal', (done) => {
        request(expressApp)
            .get('/crater/api/territoires?critere=750')
            .expect('Content-Type', /json/)
            .expect(
                200,
                [
                    {
                        id: 'metropole-du-grand-paris',
                        idParcel: null,
                        nom: 'Métropole du Grand Paris',
                        categorie: 'EPCI',
                        libellePrincipal: 'Métropole du Grand Paris',
                        libelleSecondaire: 'Intercommunalité de Paris',
                        sousCategorie: 'METROPOLE',
                        estSuggere: true
                    },
                    {
                        id: 'paris-commune',
                        idParcel: null,
                        nom: 'Paris',
                        categorie: 'COMMUNE',
                        libellePrincipal: 'Paris (75010, 75011)',
                        libelleSecondaire: 'Commune'
                    }
                ],
                done
            );
    });

    it('Test API GET /territoires retourne un territoire suggéré avant la commune', (done) => {
        request(expressApp)
            .get('/crater/api/territoires?critere=adour')
            .expect('Content-Type', /json/)
            .expect(
                200,
                [
                    {
                        id: 'communaute-de-communes-d-aire-sur-l-adour',
                        idParcel: null,
                        nom: "CC d'Aire-sur-l'Adour",
                        categorie: 'EPCI',
                        libellePrincipal: "CC d'Aire-sur-l'Adour",
                        libelleSecondaire: "Intercommunalité de Aire-sur-l'Adour",
                        sousCategorie: 'COMMUNAUTE_COMMUNES',
                        estSuggere: true
                    },
                    {
                        id: 'aire-sur-l-adour',
                        idParcel: null,
                        nom: "Aire-sur-l'Adour",
                        categorie: 'COMMUNE',
                        libellePrincipal: "Aire-sur-l'Adour (40800)",
                        libelleSecondaire: 'Commune'
                    }
                ],
                done
            );
    });

    it('Test API GET /territoires aucun territoire trouvé', (done) => {
        request(expressApp)
            .get('/crater/api/territoires?critere=12')
            .expect('Content-Type', /json/)
            .expect(200, [], done);
    });

    it('Test API GET /territoires/france', (done) => {
        request(expressApp).get('/crater/api/territoires/france').expect('Content-Type', /json/).expect(
            200,
            {
                idTerritoire: 'france',
                nomTerritoire: 'France',
                categorieTerritoire: 'PAYS',
                idParcel: '1'
            },
            done
        );
    });

    it('Test API GET /territoires/ile-de-france', (done) => {
        request(expressApp)
            .get('/crater/api/territoires/ile-de-france')
            .expect('Content-Type', /json/)
            .expect(
                200,
                {
                    idTerritoire: 'ile-de-france',
                    nomTerritoire: 'Île-de-France',
                    categorieTerritoire: 'REGION',
                    idParcel: null,
                    pays: { id: 'france', nom: 'France' }
                },
                done
            );
    });

    it('Test API GET /territoires/landes', (done) => {
        request(expressApp)
            .get('/crater/api/territoires/landes')
            .expect('Content-Type', /json/)
            .expect(
                200,
                {
                    idTerritoire: 'landes',
                    nomTerritoire: 'Landes',
                    categorieTerritoire: 'DEPARTEMENT',
                    idParcel: null,
                    pays: { id: 'france', nom: 'France' },
                    region: { id: 'nouvelle-aquitaine', nom: 'Nouvelle-Aquitaine' }
                },
                done
            );
    });

    it('Test API GET /territoires/communaute-de-communes-d-aire-sur-l-adour', (done) => {
        request(expressApp)
            .get('/crater/api/territoires/communaute-de-communes-d-aire-sur-l-adour')
            .expect('Content-Type', /json/)
            .expect(
                200,
                {
                    idTerritoire: 'communaute-de-communes-d-aire-sur-l-adour',
                    nomTerritoire: "CC d'Aire-sur-l'Adour",
                    categorieTerritoire: 'EPCI',
                    idParcel: null,
                    sousCategorieTerritoire: 'COMMUNAUTE_COMMUNES',
                    pays: { id: 'france', nom: 'France' },
                    region: { id: 'nouvelle-aquitaine', nom: 'Nouvelle-Aquitaine' },
                    departement: { id: 'landes', nom: 'Landes' }
                },
                done
            );
    });

    it('Test API GET /territoires/pnr-fictif-pour-les-tests', (done) => {
        request(expressApp)
            .get('/crater/api/territoires/pnr-fictif-pour-les-tests')
            .expect('Content-Type', /json/)
            .expect(
                200,
                {
                    idTerritoire: 'pnr-fictif-pour-les-tests',
                    nomTerritoire: 'PNR Fictif pour les tests',
                    categorieTerritoire: 'REGROUPEMENT_COMMUNES',
                    idParcel: null,
                    sousCategorieTerritoire: 'PNR',
                    pays: { id: 'france', nom: 'France' },
                    region: { id: 'occitanie', nom: 'Occitanie' },
                    departement: { id: 'gers', nom: 'Gers' }
                },
                done
            );
    });

    it('Test API GET /territoires/paris-commune', (done) => {
        request(expressApp)
            .get('/crater/api/territoires/paris-commune')
            .expect('Content-Type', /json/)
            .expect(
                200,
                {
                    idTerritoire: 'paris-commune',
                    nomTerritoire: 'Paris',
                    categorieTerritoire: 'COMMUNE',
                    idParcel: null,
                    pays: { id: 'france', nom: 'France' },
                    region: { id: 'ile-de-france', nom: 'Île-de-France' },
                    departement: { id: 'paris-departement', nom: 'Paris' },
                    epci: { id: 'metropole-du-grand-paris', nom: 'Métropole du Grand Paris' }
                },
                done
            );
    });

    it('Test API GET /territoires/unknown', (done) => {
        request(expressApp).get('/crater/api/territoires/unknown').expect('Content-Type', /json/).expect(
            404,
            {
                message: 'Ressource introuvable',
                description: 'Territoire non trouvé (idTerritoire=unknown)'
            },
            done
        );
    });

    it('Test API GET /unknown', (done) => {
        request(expressApp).get('/crater/api/unknown').expect('Content-Type', /json/).expect(
            404,
            {
                message: 'Ressource introuvable',
                description: 'Chemin inconnu : /crater/api/unknown'
            },
            done
        );
    });

    it('Test API GET /diagnostics pour France', (done) => {
        request(expressApp)
            .get('/crater/api/diagnostics/france')
            .expect('Content-Type', /json/)
            // L'objet diag est gros => plus pratique de passer par une comparaison de fichiers
            .expect((res) =>
                verifierEgaliteAvecFichierTexte(
                    JSON.stringify(res.body, null, 2),
                    CHEMIN_DONNEES_TESTS_UNITAIRES,
                    'diagnostic_france.json'
                )
            )
            .expect(200, done);
    });

    it('Test API GET /diagnostics/csv', (done) => {
        request(expressApp)
            .get('/crater/api/diagnostics/csv/france')
            .expect('Content-Type', /text\/csv/)
            .expect((res) => {
                // Le csv est est gros => plus pratique de passer par une comparaison de fichiers
                // On exclut les 5 premières lignes qui contiennent des infos contextuelles (difficiles à tester)
                let contenuFichierAValider = supprimerNPremieresLignes(res.text, 5, SAUT_LIGNE);
                verifierEgaliteAvecFichierTexte(
                    contenuFichierAValider,
                    CHEMIN_DONNEES_TESTS_UNITAIRES,
                    'diagnostic_france.csv'
                );
            })
            .expect(200, done);
    });

    it('Test API GET /diagnostics inconnu', (done) => {
        request(expressApp).get('/crater/api/diagnostics/unknown').expect('Content-Type', /json/).expect(
            404,
            {
                message: 'Ressource introuvable',
                description: 'Diagnostic non trouvé (idTerritoire=unknown)'
            },
            done
        );
    });

    it('Test API GET /error', (done) => {
        request(expressApp).get('/').expect('Content-Type', /json/).expect(404, done);
    });

    it('Test API GET /indicateurs pour un indicateur simple', (done) => {
        request(expressApp)
            .get('/crater/api/indicateurs/population?categorieTerritoire=PAYS')
            .expect('Content-Type', /json/)
            .expect(200, [{ idTerritoire: 'france', valeur: 64624945 }], done);
    });

    it('Test API GET /indicateurs pour un indicateur objet composé avec filtrage', (done) => {
        request(expressApp)
            .get(
                '/crater/api/indicateurs/intrants.pesticides.indicateursMoyennesTriennales.noduHa?annee=2019&categorieTerritoire=PAYS'
            )
            .expect('Content-Type', /json/)
            .expect(
                200,
                [
                    {
                        idTerritoire: 'france',
                        valeur: 93685898.98
                    }
                ],
                done
            );
    });

    it('Test API GET /indicateurs pour un indicateur avec valeur null', (done) => {
        request(expressApp)
            .get('/crater/api/indicateurs/population?categorieTerritoire=REGION')
            .expect('Content-Type', /json/)
            .expect(
                200,
                [
                    { idTerritoire: 'ile-de-france', valeur: null },
                    { idTerritoire: 'nouvelle-aquitaine', valeur: null },
                    { idTerritoire: 'occitanie', valeur: null }
                ],
                done
            );
    });

    it('Test API GET /indicateurs pour la categorie COMMUNE sans idsDepartements retourne une erreur', (done) => {
        request(expressApp)
            .get('/crater/api/indicateurs/population?categorieTerritoire=COMMUNE')
            .expect('Content-Type', /json/)
            .expect(400, done);
    });

    it('Test API GET /indicateurs pour la categorie COMMUNE et un departement', (done) => {
        request(expressApp)
            .get('/crater/api/indicateurs/superficieHa?categorieTerritoire=COMMUNE&idsDepartements=gers')
            .expect('Content-Type', /json/)
            .expect(200, [{ idTerritoire: 'gee-riviere', valeur: 9 }], done);
    });
});
