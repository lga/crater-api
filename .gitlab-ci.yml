variables:
  GIT_CRATER_DATA_URL: framagit.org/lga/crater-data-resultats.git

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules

# Deux déclinaisons du job fetch-crater-data-resultats pour cloner la branche adaptée depuis crater-data-resultats
# Si master => clone de master
# Si autre branche (develop ou une branche dev en cours) => clone develop
fetch-crater-data-resultats:master:
  image:
    name: alpine/git
    entrypoint: [""]
  stage: .pre
  cache: {}
  script:
    - rm -rf dist/crater-data-resultats
    - mkdir -p dist
    - git clone --single-branch -b ${CI_COMMIT_BRANCH} --depth 1 https://gitlab-ci-token:${CI_JOB_TOKEN}@${GIT_CRATER_DATA_URL} dist/crater-data-resultats
    - cd dist/crater-data-resultats
    - ls -A1 | grep -v data | xargs rm -rf
  variables:
    GIT_STRATEGY: none
  artifacts:
    untracked: false
    expire_in: 1h
    paths:
      - "dist/crater-data-resultats"
  only:
    - master

fetch-crater-data-resultats:autres-branches:
  image:
    name: alpine/git
    entrypoint: [""]
  stage: .pre
  cache: {}
  script:
    - rm -rf dist/crater-data-resultats
    - mkdir -p dist
    - git clone --single-branch -b develop --depth 1 https://gitlab-ci-token:${CI_JOB_TOKEN}@${GIT_CRATER_DATA_URL} dist/crater-data-resultats
    - cd dist/crater-data-resultats
    - ls -A1 | grep -v data | xargs rm -rf
  variables:
    GIT_STRATEGY: none
  artifacts:
    untracked: false
    expire_in: 1h
    paths:
      - "dist/crater-data-resultats"
  except:
    - master

build:
  # Image avec JDK 8 et Node 14 (lts)
  image: timbru31/java-node:8-14
  stage: build
  dependencies:
    - "fetch-crater-data-resultats:master"
    - "fetch-crater-data-resultats:autres-branches"
  script:
    - npm ci
    - npm run package
    - npm run test
  artifacts:
    expire_in: 1 week
    paths:
      - dist/packaged

.before_script_deploy:
  before_script:
    # pour lftp
    - apk --no-cache add lftp ca-certificates openssh
    # config clé ssh, voir https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
    - 'command -v ssh-agent >/dev/null || ( apk update && apk add openssh-client )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    # Complément pour désactiver la vérification de clé du serveur distant OVH (Voir https://docs.gitlab.com/ee/ci/ssh_keys/)
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config'

deploy-prod:
  extends: .before_script_deploy
  image: alpine
  stage: deploy
  only:
    - master
  script:
    - lftp -c "set ftp:ssl-allow no; open -u $O2S_FTP_USERNAME,$O2S_FTP_PASSWORD $O2S_FTP_HOST; mirror -Rev dist/packaged ./crater/app/crater-api --parallel=10 --exclude-glob .git* --exclude .git/"
    # lftp veut un MDP, mais il n'est pas utilisé (clé ssh à la place), on met donc un MDP bidon
    - lftp -c "open -u $OVH_FTP_USERNAME,MDP_BIDON sftp://$OVH_FTP_HOST; mirror -Rev dist/packaged ./crater/prod/crater-api --parallel=10 --exclude-glob .git* --exclude .git/"
    - lftp -c "open -u $OVH_FTP_USERNAME,MDP_BIDON sftp://$OVH_FTP_HOST; mirror -Rev ovh ./crater/ovh --parallel=10 --exclude-glob .git* --exclude .git/"
    - ssh $OVH_FTP_USERNAME@$OVH_FTP_HOST "cd crater/ovh/scripts && ./installer-prod.sh"

deploy-dev:
  extends: .before_script_deploy
  image: alpine
  stage: deploy
  only:
    - develop
  script:
    - lftp -c "set ftp:ssl-allow no; open -u $O2S_FTP_USERNAME,$O2S_FTP_PASSWORD $O2S_FTP_HOST; mirror -Rev dist/packaged ./crater/dev/crater-api --parallel=10 --exclude-glob .git* --exclude .git/"
    # lftp veut un MDP, mais il n'est pas utilisé (clé ssh à la place), on met donc un MDP bidon
    - lftp -c "open -u $OVH_FTP_USERNAME,MDP_BIDON sftp://$OVH_FTP_HOST; mirror -Rev dist/packaged ./crater/dev/crater-api --parallel=10 --exclude-glob .git* --exclude .git/"
    - lftp -c "open -u $OVH_FTP_USERNAME,MDP_BIDON sftp://$OVH_FTP_HOST; mirror -Rev ovh ./crater/ovh --parallel=10 --exclude-glob .git* --exclude .git/"
    - ssh $OVH_FTP_USERNAME@$OVH_FTP_HOST "cd crater/ovh/scripts && ./installer-dev.sh"
